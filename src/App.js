import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import logo from './logo.svg';
import './styles/App.scss';
import './styles/override.scss';

import { Headers } from './components';
import { HomeContainer, ProductDetail, LoginContainer } from './containers';
class App extends Component {
  _showLoginModal = () => {
    window.open("/login", "", "width=1280, height=500");
  }

  render() {
    return (
      <Router>
        <div className="container-fluid padd-none">
          <Headers showLoginFunc={this._showLoginModal} />
          <Route exact path="/" component={HomeContainer} />
          <Route exact path="/productdetail" component={ProductDetail} />
          <Route exact path="/login" component={LoginContainer} />
        </div>
      </Router>
    );
  }
}

export default App;
