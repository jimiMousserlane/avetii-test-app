import React from 'react'

const headerMenuList = [
  {name:"Home", linkTo: "#"},
  {name:"About us", linkTo: "#"},
  {name:"Products", linkTo: "#"},
  {name:"Blog", linkTo: "#"},
  {name:"News", linkTo: "#"},
  {name:"Support", linkTo: "#"},
  {name:"Contact", linkTo: "#"},
];

export const Headers = ({ showLoginFunc }) => {
  return (
    <header className="row padd-hor-base padd-ver-base">
      <div id="header-main-container" className="col-xs-12">
        <div id="header-logo-left" className="col-xs-4">
          <a href="/">
            <img src="/assets/logoac.png" alt="Avetti Commerce" />
          </a>
        </div>

        <div id="header-section-center" className="col-xs-8">
          <div className="col-xs-12 marg-ver-base">

            <div className="col-xs-2 col-xs-push-6" style={{ textAlign: 'end' }}>
              <p>
                View Cart
              </p>
            </div>
            <div className="col-xs-2 col-xs-push-6">
              <p>
                Order History
              </p>
            </div>
            <div className="col-xs-2 col-xs-push-7">
              <a style={{ textDecoration: 'none', color: 'black' }} href="#">
                <p onClick={showLoginFunc}>
                  Login
                </p>
              </a>
            </div>
            
          </div>
          
          <div className="col-xs-12 marg-ver-base">
            <div className="col-xs-4 col-xs-push-7 padd-none">
              <Search />
            </div>
            <div className="col-xs-4 col-xs-push-7">
              <button className="btn btn-dark my-btn-rounded">
                Search
              </button>
            </div>
          </div>
          
          <div className="col-xs-12 marg-ver-base">
            <ul className="list-inline header-menulist col-xs-9">
              {Array.isArray(headerMenuList) && headerMenuList.length > 0 && headerMenuList.map((menu, key) =>
                <li key={key}>
                  <a href={menu.linkTo}>
                    {menu.name}
                  </a>
                </li>
              )}
            </ul>
            <div className="col-xs-3" style={{ textAlign: 'end' }}>
              <p>
                800-888-8888
              </p>
            </div>
          </div>
        </div>

      </div>
    </header>
  );
}

export const Search = ({ textinput, onChangeFunc }) => {
  return (
    <input
      className="padd-hor-base"
      placeholder="Products, Parts, Keywords Search"
      style={{ width: '100%', border: '1px solid #d6d6d6', paddingTop: 5, paddingBottom: 5 }}
    />
  );
}