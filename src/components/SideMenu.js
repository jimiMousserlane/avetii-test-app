import React from 'react'
import sideMenuItems from '../contents/dummyData/sidemenu-data.json';

export const SideMenu = (props) => {
  return (
    <div className="sidemenu col-xs-5 marg-ver-base">
      <ul className="list-group marg-none">
        {Array.isArray(sideMenuItems) && sideMenuItems.map((menu, key) => 
          <a className="list-group-item list-group-item-action" key={key} href="#">
            {menu.name}
          </a>    
        )}
      </ul>
    </div>
  );
}