import React from 'react'
import { Link } from 'react-router-dom';

export const ProductCard = ({ idx, productdata }) => {
  const isOdd = idx % 2 === 1;
  return (
    <div id={`prodcard-${idx}`} className="col-xs-6" style={{ paddingLeft: 0, paddingRight: isOdd ? 0 : 10 }}>
      <div className="product-card col-xs-12" style={{ padding: 5, marginRight: 5, marginTop: 5 }}>
        <div className="col-xs-3 padd-none">
          <img
            src={productdata.imageUrl}
            className="img-thumbnail" alt={productdata.title}
            style={{ width: 150 }}
            />
        </div>
        <div className="col-xs-9 padd-hor-base">
          <h4>
            {productdata.title}
          </h4>
          <h4>
            {productdata.SKU}
          </h4>
          <p>
            {productdata.description}
          </p>
          <Link to="/productdetail">
            <button className="btn btn-dark my-btn-rounded" style={{ borderRadius: 20, paddingLeft: 15, paddingRight: 15 }}>
              More Info&nbsp;>>
            </button>
          </Link>
        </div>
      </div>
    </div>
  )
}