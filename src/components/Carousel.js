import React from 'react';
import Slider from 'react-slick';

export const Carousel = ({ images }) => {
  const setting = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  }

  return (
    <Slider {...setting}>
      {Array.isArray(images) && images.map((image, key) =>
        <div key={key}>
          <img
            src={image.imgsource}
            alt={image.imgsource}
            className="img-responsive"
            style={{ height: 440 }}
          />
        </div>
      )}
    </Slider>
  );
}