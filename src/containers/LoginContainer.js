import React, { Component } from 'react'

class LoginContainer extends Component {
  state = {
    email: '',
    password: ''
  }

  _onChangeForm = (state, event) => {
    console.log(event.target.value);
    const { value } = event.target;
    this.setState({
      [state]: value
    });
  }

  _validateForm = () => {
    const { email, password } = this.state;
    const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/);
    if (!email || !password) {
      alert('Email or Password must not be empty');
    } else if (!emailRegex.test(email)) {
      alert('Bad Email Format');
    } else {
      alert('Data Submision complete');
      window.close();
    }
  }

  render() {
    const { email, password } = this.state;
    return (
      <form className="container">
        <div className="col-xs-12">
          <input style={{ width: '50%' }} placeholder="email" type="email" value={email} onChange={(e) => this._onChangeForm('email', e)} />
        </div>
        <div className="col-xs-12">
          <input style={{ width: '50%' }} placeholder="Password" type="password" value={password} onChange={(e) => this._onChangeForm('password', e)} />
        </div>
        <input type="submit" onClick={this._validateForm}/>
      </form>
    );
  }
}

export { LoginContainer };