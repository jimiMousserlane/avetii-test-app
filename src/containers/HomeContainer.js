import React, { Component } from 'react'
import { SideMenu, Carousel, ProductCard } from '../components';
import productdata from '../contents/dummyData/products-data.json';
import carouseldata from '../contents/dummyData/jumbotron-data.json';

class HomeContainer extends Component {
  render() {
    return (
      <div>
        <div id="jumbotron-menu-container" className="col-xs-12 padd-none">
          <SideMenu />
          <Carousel images={carouseldata} />
        </div>

        <article id="product-section" className="col-xs-12 marg-ver-base padd-ver-base">
          <div className="col-xs-8 padd-none">
            <div className="padd-hor-base padd-ver-base" style={{ border: '1px solid #d6d6d6' }}>
              <h4>
                Featured Products
              </h4>
            </div>
            {Array.isArray(productdata) && productdata.map((product, key) =>
              <div key={key} className="marg-ver-base">
                <ProductCard
                  productdata={product}
                  idx={key}
                />
              </div>
            )}
          </div>
          <div className="col-xs-4">
            <div className="col-xs-12 marg-hor-base" style={{ border: '1px solid #d6d6d6', minHeight: 400 }} />
          </div>
        </article>
      </div>
    );
  }
}

export { HomeContainer };