import React, { Component } from 'react'
import { SideMenu } from '../components';
import productdetail from '../contents/dummyData/product-detail-data.json';

class ProductDetail extends Component {
  state = {
    selectedImage: null
  }

  componentDidMount() {
    this.setState({
      selectedImage: productdetail.productImages[0].imageUrl
    })
  }

  _onImageClick = (index) => {
    this.setState({
      selectedImage: productdetail.productImages[index].imageUrl
    })
  }

  render() {
    const { selectedImage } = this.state;
    return (
      <div className="col-xs-12">
        <SideMenu />
        <div className="col-xs-8">
          <div id="banner" className="col-xs-12 padd-none" style={{ maxHeight: 150 }}>
            <img
              alt="banner"
              src="https://images.unsplash.com/photo-1470267454059-db1a8ce09166?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1a41898231fa767e85b49092fd7d5c0f&auto=format&fit=crop&w=1950&q=80"
              style={{ maxHeight: 150, width: '100%' }}
            />
          </div>
          <div className="col-xs-12 padd-none">
            <h3>
              Featured Product
            </h3>
            <p>
             {productdetail.productdetaildesc}
            </p>
          </div>
          <div className="col-xs-12 padd-none">
            <div className="col-xs-6 padd-none">
              <div className="col-xs-12 padd-none">
                <div id="image-wrapper" style={{ maxHeight: 200 }}>
                  <img src={selectedImage} className="img-responsive" alt="Main" style={{ height: 200, width: '100%' }} />
                </div>
              </div>
              
              <div className="col-xs-12 padd-none marg-ver-base">
                <ul className="list-inline">
                {Array.isArray(productdetail.productImages) && productdetail.productImages.map((image, key) =>
                  <li key={key} onClick={() => this._onImageClick(key)} className="col-xs-3" style={{ maxHeight: 100 }}>
                    <img src={image.imageUrl} className="img-thumbnail" alt="display" style={{ height: 100 }}  />
                    <p style={{ textAlign: 'center' }} className="marg-ver-base">
                      {image.name}
                    </p>
                  </li>
                )}
                </ul>
              </div>
            </div>
            
            <div className="col-xs-6">
              <h4 className="col-xs-12">
                {productdetail.productTitle}
              </h4>
              <h5 className="col-xs-12">
                SKU
              </h5>
              <p className="col-xs-12">
                {productdetail.productdetaildesc}
              </p>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export { ProductDetail };
